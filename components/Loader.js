import React , { Component }  from 'react';
import { Animated, StyleSheet, Text, View, Image, ImageBackground} from 'react-native';

export default class Loader extends React.Component {

constructor(){
super()
this.state ={}
this.state.customStyles = {
opacity:1
}

setInterval(()=>{
if (this.state.customStyles.opacity == 1 ){
  this.setState({
    customStyles:{
      opacity:0.5
    }
  })
}else{
  this.setState({
    customStyles:{
      opacity:1
    }
  })
}
}, 1000)
}

  render(){
  return (
    <ImageBackground source={require('./assets/farmbersbg.jpg')} style={styles.container}>
      <Image source={require('./assets/farmberslogo.png')}  style={[styles.image , this.state.customStyles]} />

    </ImageBackground>
    
  );
}
}


const styles = StyleSheet.create({
    container: {
      flex: 1,
      alignItems: 'center',
      justifyContent: 'center',
    },
  image:{
    opacity:.5,
  }
  
  });
  