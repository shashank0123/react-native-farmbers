import React, { Component } from 'react';
import 'react-native-gesture-handler';
import {NavigationContainer} from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import {
  StyleSheet,
  Text,
  View,
  TextInput,
  Button,
  TouchableHighlight,
  Image,
  Linking,
  ImageBackground,
  Alert
} from 'react-native';

export default class App extends Component {

  constructor(props) {
    super(props);
    state = {
      email   : '',
      password: '',
    }
  }

  onClickListener = (viewId) => {
    Alert.alert("Alert", "Button pressed "+viewId);
  }

  render() {
    return (
      <ImageBackground source={require('./assets/farmbersbg.jpg')} style={styles.imagebackground}>
         <Image source={require('./assets/farmberslogo.png')}  style={styles.image} />
      <View style={styles.container}>
        <View style={styles.inputContainer1}>
          <Image style={styles.inputIcon} source={require('./assets/Email.png')}/>
          <TextInput style={styles.inputs}
              placeholder="Username"
              placeholderTextColor = "#000"
              keyboardType="email-address"
              underlineColorAndroid='transparent'
              onChangeText={(email) => this.setState({email})}/>
        </View>
        
        <View style={styles.inputContainer}>
          <Image style={styles.inputIcon} source={require('./assets/Password.png')}/>
          <TextInput style={styles.inputs}
              placeholder="Password"
              secureTextEntry={true}
              placeholderTextColor = "#000"
              underlineColorAndroid='transparent'
              onChangeText={(password) => this.setState({password})}/>
        </View>


        <TouchableHighlight style={styles.forgetpassword} >
        <Text style={styles.TextStyle1} onPress={ ()=> Linking.openURL('https://google.com') } >Forget Password?</Text>
        </TouchableHighlight>

        <TouchableHighlight style={[styles.buttonContainer, styles.loginButton]} onPress={() => this.onClickListener('login')}>
          <Text style={styles.loginText}>LOGIN</Text>
        </TouchableHighlight>

        <TouchableHighlight style={styles.buttonContainer1} >
            <Text style={styles.TextStyle}>Signup With</Text>
        </TouchableHighlight>
        
<View style={styles.socialconatiner}>
<Image style={styles.social} source={require('./assets/facebook.png')}/>
<Image style={styles.social} source={require('./assets/google.png')}/>
</View>

<View style={styles.bottomtext}>
<Text style={styles.bottomtext1}>Don't have an account</Text>
<Text style={styles.bottomtext2}>sign up</Text>
</View>

      </View>
      </ImageBackground>
    );
  }
}

const styles = StyleSheet.create({

  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    marginTop:100,
  },
  bottomtext1:{
fontSize:18,
  },
  bottomtext2:{
color:'red',
marginLeft:8,
fontSize:18,

  },
  socialconatiner:{
    display:'flex',
    flexDirection:'row',
  },
bottomtext:{
display:'flex',
flexDirection:'row',
marginTop:100,
},

  social:{
  width:40,
  height:40,
margin:8,
  },
forgetpassword:{
marginTop:15,
marginLeft:100,

},
TextStyle1:{
  fontWeight:'700',
  fontSize:16,
},
TextStyle:{
  fontWeight:'900',
  fontSize:16,
},

  imagebackground: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  image:{
  top:130
  },
  inputContainer1: {
    borderColor:'#000',
      backgroundColor: '#FFFFFF00',
      borderRadius:30,
      borderWidth: 2,
      width:300,
      height:60,
      marginBottom:20,
     
      flexDirection: 'row',
      alignItems:'center'
  },
  inputContainer: {
    borderColor:'#000',
      backgroundColor: '#FFFFFF00',
      borderRadius:30,
      borderWidth: 2,
      width:300,
      height:60,
    
      flexDirection: 'row',
      alignItems:'center'
  },
  placeholder:{
    color:'#000',
  },
  inputs:{
      height:45,
      marginLeft:10,
      flex:1,
      color:'#000',
      borderColor:'#000',
  },
  inputIcon:{
    width:25,
    height:25,
    marginLeft:15,
    justifyContent: 'center'
  },
  buttonContainer: {
    height:50,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom:20,
    marginTop:40,
    width:150,
    borderRadius:30,
  },
  loginButton: {
    backgroundColor: "#115411",
  },
  loginText: {
    color: '#fff',
    backgroundColor: "#115411",
  }
});